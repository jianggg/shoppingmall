import React, { useState } from "react";
import "./App.css";

function App() {
  const clothData = [
    {
      type: "shirt",
      src: "/pink_t.png",
      gender: "female",
      size: "large",
      color: "pink",
    },
    {
      type: "pants",
      src: "/blue_p.png",
      gender: "man",
      size: "small",
      color: "blue",
    },
    {
      type: "pants",
      src: "/yellow_p.png",
      gender: "man",
      size: "large",
      color: "yellow",
    },
    {
      type: "skirt",
      src: "/yellow_s.png",
      gender: "female",
      size: "large",
      color: "yellow",
    },
    {
      type: "shirt",
      src: "/yellow_t.png",
      gender: "man",
      size: "small",
      color: "yellow",
    },
    {
      type: "skirt",
      src: "/blue_s.png",
      gender: "female",
      size: "small",
      color: "blue",
    },
    {
      type: "shirt",
      src: "/blue_t.png",
      gender: "man",
      size: "small",
      color: "blue",
    },
    {
      type: "shirt",
      src: "/yellow_t.png",
      gender: "man",
      size: "small",
      color: "yellow",
    },
    {
      type: "pants",
      src: "/pink_p.png",
      gender: "man",
      size: "small",
      color: "pink",
    },
    {
      type: "pants",
      src: "/pink_p.png",
      gender: "female",
      size: "small",
      color: "pink",
    },
  ];
  const [listData, setList] = useState([...clothData]);
  function filterData(key, value) {
    const newData = clothData.filter((item) => item[key] === value);
    console.log(newData);
    setList(newData);
  }
  return (
    <div className="App">
      <header className="header">
        <img className="logo" src="/logo.png" alt="logo"></img>
      </header>
      <nav>
        <ul className="nav">
          <li
            className="image shirt"
            onClick={() => filterData("type", "shirt")}
          ></li>
          <li
            className="image pants"
            onClick={() => filterData("type", "pants")}
          ></li>
          <li
            className="image skirt"
            onClick={() => filterData("type", "skirt")}
          ></li>
          <li
            className="color blue"
            onClick={() => filterData("color", "blue")}
          >
            Blue
          </li>
          <li
            className="color yellow"
            onClick={() => filterData("color", "yellow")}
          >
            Yellow
          </li>
          <li
            className="color pink"
            onClick={() => filterData("color", "pink")}
          >
            Pink
          </li>
        </ul>
      </nav>
      <main className="listContainer">
        {listData.map((item, index) => (
          <div className="listItem" key={index}>
            <img className="itemImg" src={item.src} alt="clothes"></img>
            <span className="gender">{item.gender}, </span>
            <span className="size"> {item.size} size</span>
          </div>
        ))}
      </main>
    </div>
  );
}

export default App;
